class UserMailer < ApplicationMailer
    
    def message_stats_email(user)
        
        @user = user
        time_range = (user.messages.last.created_at)..(Time.now)
        @last_week_message_count = Message.where(created_at: (Date.today.last_week.beginning_of_week)..(Date.today.last_week.at_end_of_week)).count
        @message_count_since_last_message = Message.where.not(user_id: user.id).where(created_at: time_range).count
        mail(to: user.email, subject: "Message stats", from: "Chat App <contato@ensinohi.com.br>")
        
    end
  
end
