desc "This task is called by the Heroku scheduler add-on"

task :send_message_stats => :environment do
  
  users_to_send_email = User.all
  users_to_send_email.each do |user|
    UserMailer.message_stats_email(user).deliver_now
  end
   
end

